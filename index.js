var five = require('johnny-five')
var pixel = require('node-pixel')

var board = new five.Board({repl: false})
var motion
var strip

var neopixelsReady
var targetBrightness = 0
var currentBrightness
var stId // timeout id

var timeToFade = 30 * 10000
var stripLength = 30
var pixels = []

// Init pixels.
for (var i = 0; i < stripLength; i++) {
  pixels[i] = 0
}

board.on('ready', onBoardReady)

function onBoardReady () {
  console.log('Board is ready.')
  setupMotionSensor()
}

function setupMotionSensor () {
  console.log('Setting up motion sensor.')
  motion = new five.Motion(2)
  motion.on('calibrated', onMotionSensorReady)
}
function onMotionSensorReady () {
  console.log('Motion sensor is ready.')

  // motion.on('motionend', onMotionEnd)
  setupNeopixels()
}
function onMotionStart () {
  console.log('I see something.')
  targetBrightness = 255
  clearTimeout(stId)
  stId = setTimeout(setTargetBrightnessToZero, timeToFade)
}

function setTargetBrightnessToZero () {
  console.log('Setting target brightness to zero.')
  targetBrightness = 0
}

function setupNeopixels () {
  console.log('Setting up Neopixels.')
  strip = new pixel.Strip({
    board: board,
    controller: 'FIRMATA',
    strips: [{pin: 6, length: 30}]
  })

  strip.on('ready', onNeopixelsReady)
}

function onNeopixelsReady () {
  console.log('Neopixels are ready.')
  motion.on('motionstart', onMotionStart)
  setInterval(tick, 500)
  neopixelsReady = true
}

function tick () {
  var center = pixels.length / 2
  var minDamping = 10
  var maxDamping = 100
  var diffDamping = maxDamping - minDamping

  for (var i = 0; i < pixels.length; i++) {
    // compute distance from center.
    var dist = Math.abs(i - center)
    var distPer = dist / center
    var damping = minDamping + (diffDamping * distPer)
    var pixelB = pixels[i]
    var deltaB = targetBrightness - pixelB
    var velocityB = deltaB / damping

    pixels[i] = pixelB + velocityB
    console.log(i, parseInt(pixels[i], 10))
    var p = strip.pixel(i)
    p.color([0, parseInt(pixels[i], 10), 0])
  }
  strip.show()
}
